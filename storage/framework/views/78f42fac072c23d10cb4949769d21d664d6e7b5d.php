<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e($pageTitle); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->getFromJson('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.credit-discredit')); ?>"><?php echo e($pageTitle); ?></a></li>
                <li class="active"><?php echo e('View Causes'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">

         <div class="col-md-12">

              <div class="white-box p-b-0 bg-inverse text-white">
                 <div class="row">
                      <form method="GET" action="/admin/credit-discredit-cause/<?php echo e($id); ?>" >

                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="control-label">Starting Date</label>
                                  <input type="text" class="form-control" name="starting_date" id="credit_discredit_starting_date" value="<?php if(isset($starting_date)): ?><?php echo e($starting_date); ?><?php else: ?><?php echo e(Carbon\Carbon::today()->format('Y-m-d')); ?><?php endif; ?>" onchange="checkstart()">
                              </div>

                          </div>
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="control-label">Ending Date</label>
                                  <input type="text" class="form-control" name="ending_date" id="credit_discredit_ending_date" value="<?php if(isset($ending_date)): ?><?php echo e($ending_date); ?><?php else: ?><?php echo e(Carbon\Carbon::today()->format('Y-m-d')); ?><?php endif; ?>" onchange="checkend()">
                              </div>

                          </div>

                          <div class="col-md-3">
                               <div class="form-group m-t-25">
                                    <button type="submit" id="apply-filter" class="btn btn-success btn-block">Apply</button>
                               </div>
                          </div>
                     </form>



                 </div>

             </div>
             <div class="white-box">


                  <div class="table-responsive">
                       <div class="white-box">
                            <div class="row">
                                 <div class="col-md-4"></div>
                                 <div class="col-md-4">
                                      <div class="col-md-4"><b> User Id :</b> <label class="label label-success"> <?php echo e($id); ?></label></div>
                                      <?php 
                                      $employee=DB::table('users')->where('id',$id)->get();
                                       ?>
                                      <div class="col-md-8"> <b> User Name : </b> <?php if(isset($employee)): ?>
                                           <?php if(count($employee)>0): ?><label class="label label-primary"><?php echo e($employee[0]->name); ?></label><?php endif; ?> <?php endif; ?> </div>

                                           </div>
                                      </div>
                                 </div>
                                 <div class="white-box">
                                    <div class="row">


                        <div class="col-md-3">

                        </div>

                        <div class="col-md-3">
                            <div class="white-box bg-success">
                                <h3 class="box-title text-white">Total Credit</h3>
                                <ul class="list-inline two-part">
                                    <li><i class="icon-clock text-white"></i></li>
                                    <li class="text-right"><span id="daysLate" class="counter text-white">
                                         <?php 
                                              $credit_point=0;
                                          ?>
                                         <?php if(isset($starting_date)): ?>

                                        <?php if(isset($ending_date)): ?>
                                             <?php 
                                             $credit=DB::table('employee_credit_discredit')->where('user_id',$id)->where('type',1)->where('created_at','>=',$starting_date.' 00:00:00')->where('created_at','<=',$ending_date.' 23:59:59')->get();
                                              ?>
                                        <?php else: ?>
                                             <?php 
                                             $credit=DB::table('employee_credit_discredit')->where('user_id',$id)->where('type',1)->get();
                                              ?>


                                        <?php endif; ?>
                                        <?php else: ?>
                                             <?php 
                                             $credit=DB::table('employee_credit_discredit')->where('user_id',$id)->where('type',1)->get();
                                              ?>
                                        <?php endif; ?>
                                        <?php if(isset($credit)): ?>
                                        <?php if(count($credit)>0): ?>
                                         <?php $__currentLoopData = $credit; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $credit): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                              <?php 
                                              $credit_point=$credit_point+$credit->point;
                                               ?>
                                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                           <?php endif; ?>
                                           <?php endif; ?>
                                           <?php echo e($credit_point); ?>

                                    </span></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="white-box bg-danger">
                                <h3 class="box-title text-white">Total Discredit</h3>
                                <ul class="list-inline two-part">
                                    <li><i class="icon-clock text-white"></i></li>
                                    <li class="text-right"><span id="halfDays" class="counter text-white">
                                         <?php 
                                              $discredit_point=0;
                                          ?>

                                                                      <?php if(isset($starting_date)): ?>

                                                                   <?php if(isset($ending_date)): ?>
                                                                        <?php 
                                                                       $discredit=DB::table('employee_credit_discredit')->where('user_id',$id)->where('type',2)->where('created_at','>=',$starting_date.' 00:00:00')->where('created_at','<=',$ending_date.' 23:59:59')->get();
                                                                         ?>
                                                                   <?php else: ?>
                                                                        <?php 
                                                                       $discredit=DB::table('employee_credit_discredit')->where('type',2)->where('user_id',$id)->get();
                                                                         ?>


                                                                   <?php endif; ?>
                                                                   <?php else: ?>
                                                                        <?php 
                                                                        $discredit=DB::table('employee_credit_discredit')->where('type',2)->where('user_id',$id)->get();
                                                                         ?>
                                                                   <?php endif; ?>
                                                                   <?php if(isset($discredit)): ?>
                                                                   <?php if(count($discredit)>0): ?>

                                                                     <?php $__currentLoopData = $discredit; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $discredit): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                          <?php 
                                                                          $discredit_point=$discredit_point+$discredit->point;
                                                                           ?>
                                                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                <?php endif; ?>
                                                                <?php endif; ?>
                                                                <?php echo e($discredit_point); ?>

                                    </span></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                    </div>
                 <?php if(isset($starting_date)): ?>

              <?php if(isset($ending_date)): ?>
                   <?php 
                  $causes=DB::table('employee_credit_discredit')->where('user_id',$id)->where('created_at','>=',$starting_date.' 00:00:00')->where('created_at','<=',$ending_date.' 23:59:59')->get();
                    ?>
              <?php else: ?>
                   <?php 
                  $causes=DB::table('employee_credit_discredit')->where('user_id',$id)->get();
                    ?>


              <?php endif; ?>
              <?php else: ?>
                   <?php 
                   $causes=DB::table('employee_credit_discredit')->where('user_id',$id)->get();
                    ?>
              <?php endif; ?>
              <?php if(isset($causes)): ?>
              <?php if(count($causes)>0): ?>




                    <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="users-table">
                        <thead>
                        <tr>
                            <th>Credit/Discredit</th>
                            <th>Point</th>
                            <th>Given By</th>
                            <th>Cause</th>
                            <th>Date</th>
                            <th><?php echo app('translator')->getFromJson('app.action'); ?></th>
                        </tr>
                        </thead>
                        <?php $__currentLoopData = $causes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cause): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <tr>
                             <td><?php if($cause->type==2): ?><label class="label label-danger">Discredit</label><?php elseif($cause->type==1): ?><label class="label label-success">Credit</label><?php endif; ?></td>
                              <td><?php echo e($cause->point); ?></td>
                              <td><?php echo e($cause->given_by); ?></td>
                              <td>
                                   <?php 
                                   $cause_text=DB::table('credit_discredit_cause')->where('id',$cause->cause)->get();
                                    ?>
                                   <?php if(isset($cause_text)): ?>
                                       <?php if(count($cause_text)>0): ?><?php echo e($cause_text[0]->cause_details); ?><?php endif; ?> <?php endif; ?>
                                            <?php if(is_null($cause->specific_cause)): ?>

                                            <?php else: ?>
                                              <div>   <h6 class="label label-primary"><?php echo e($cause->specific_cause); ?></h6> </div>
                                            <?php endif; ?>
                                   </td>
                              <td>
                                   <?php  $date = new DateTime($cause->created_at);$date = $date->format('d-m-Y'); ?><?php echo e($date); ?>

                              </td>
                              <td><a href="/admin/credit-discredit/edit/<?php echo e($cause->id); ?>" class="btn btn-info btn-circle" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      <a href="/admin/credit-discredit/delete/<?php echo e($cause->id); ?>" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn-circle sa-params" data-toggle="tooltip" data-user-id="1" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a></td>

                        </tr>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </table>
                </div>
           <?php endif; ?>
           <?php endif; ?>
            </div>
        </div>
    </div>
    <!-- .row -->

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script src="<?php echo e(asset('plugins/bower_components/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script>
jQuery('#credit_discredit_starting_date').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd',
    todayHighlight: true,
    endDate: '+0d'
}).on('changeDate', function (ev) {
    showTable();
});
var table;

function showTable(){
   table = $('#attendance-table').dataTable({

        "destroy" : true
   });
}
jQuery('#credit_discredit_ending_date').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd',
    todayHighlight: true,
    endDate: '+0d'
}).on('changeDate', function (ev) {
    showTableOne();
});
var table;

function showTableOne(){
   table = $('#attendance-table').dataTable({

        "destroy" : true
   });
}
function checkstart(){
     var x = $.datepicker.formatDate("yy-mm-dd", new Date($('#credit_discredit_starting_date').val()));
     var y = $.datepicker.formatDate("yy-mm-dd", new Date($('#credit_discredit_ending_date').val()));

     if(x>y){
          $('#credit_discredit_ending_date').val(x);
     }

}
function checkend(){
     var x = $.datepicker.formatDate("yy-mm-dd", new Date($('#credit_discredit_starting_date').val()));
     var y = $.datepicker.formatDate("yy-mm-dd", new Date($('#credit_discredit_ending_date').val()));

     if(x>y){
          $('#credit_discredit_starting_date').val(y);
     }
}
</script>

<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>