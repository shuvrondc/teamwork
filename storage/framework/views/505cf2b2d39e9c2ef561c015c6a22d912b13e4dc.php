<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e($pageTitle); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->getFromJson('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.credit-discredit')); ?>"><?php echo e($pageTitle); ?></a></li>
                <li class="active"><?php echo e('Add Cause'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('head-script'); ?>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/summernote/dist/summernote.css')); ?>">
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
         <div class="col-md-8">
         </div>
         <div class="col-md-4">

            <div class="jq-toast-wrap top-right">
                 <div class="success_msg jq-toast-single jq-has-icon jq-icon-success" style="text-align: left; display: none;">
                      <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 3.1s ease-in;-o-transition: width 3.1s ease-in;transition: width 3.1s ease-in;background-color: #ff6849;"></span>
                      <?php if($errors->any()): ?>
                           <?php echo e($errors->first()); ?>

                      <?php endif; ?>
            </div>

         </div>

         </div>

        <div class="col-md-12">


            <div class="panel panel-inverse">
                <div class="panel-heading"> Add Cause</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                       <form method="GET" action="/admin/credit-discredit/createcause" >
                            <input type="hidden" name="type" value="1">
                        <div class="form-body">

                            <div class="row">
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                         <label class="control-label">Select Credit/Discredit</label>

                                        <select class="selectpicker form-control" name="type" id="category_id"
                                                data-style="form-control">
                                                <option value="1"><?php echo e("Credit"); ?></option>
                                                <option value="2"><?php echo e("Discredit"); ?></option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                            <div class="row">
                                 <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Cause Details</label>
                                        <textarea name="cause_details" id="notes" rows="5" class="form-control"></textarea>
                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                <!--/span-->


                                <!--/span-->
                            </div>
                            <!--/row-->


                        </div>
                        <div class="form-actions">
                            <button type="submit" id="save-form" class="btn btn-success"><i class="fa fa-check"></i>
                                Add
                            </button>
                        </div>
                   </form>
                    </div>
                </div>
            </div>
            <div class="panel panel-inverse">
                <div class="panel-heading"> Recently Added Cause</div>

            <div class="white-box">

                  <?php 
                  $causes=DB::table('credit_discredit_cause')->orderBy('created_at','Desc')->get();
                   ?>
               <?php if(isset($causes)): ?>
               <?php if(count($causes)>0): ?>


                <div class="table-responsive">
                    <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="users-table">
                        <thead>
                        <tr>
                           <th>Id</th>
                           <th>Credit/Discredit</th>

                           <th>Cause Details</th>
                           <th>Date</th>
                           <th><?php echo app('translator')->getFromJson('app.action'); ?></th>
                        </tr>
                        </thead>
                        <?php $__currentLoopData = $causes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cause): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <tr>
                             <td><?php echo e($cause->id); ?></td>
                             <td><?php if($cause->type==2): ?><label class="label label-danger">Discredit</label><?php elseif($cause->type==1): ?><label class="label label-success">Credit</label><?php endif; ?></td>
                              <td><?php echo e($cause->cause_details); ?></td>

                              <td>
                                   <?php  $date = new DateTime($cause->created_at);$date = $date->format('d-m-Y'); ?><?php echo e($date); ?>

                              </td>
                              <td>
                                   <a href="/admin/credit-discredit/editcause/<?php echo e($cause->id); ?>" class="btn btn-info btn-circle" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                      <a href="/admin/credit-discredit/deletecause/<?php echo e($cause->id); ?>" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn-circle sa-params" data-toggle="tooltip" data-user-id="1" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a></td>

                        </tr>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </table>
                </div>
               <?php endif; ?>
               <?php endif; ?>
               </div>
          </div>
          </div>
        </div>
    </div>    <!-- .row -->

  <?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/summernote/dist/summernote.min.js')); ?>"></script>
<?php if($errors->any()): ?>
<script>
$('.success_msg').css("display", "block");;
      $('.success_msg').delay(2000).fadeOut('slow');
                setTimeout(function() {
                     $(".success_msg").modal('hide');
      }, 2500);
      </script>
<?php endif; ?>

<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>