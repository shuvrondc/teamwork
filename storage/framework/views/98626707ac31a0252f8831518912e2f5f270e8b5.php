<div class="rpanel-title"> <?php echo app('translator')->getFromJson('app.task'); ?> <span><i class="ti-close right-side-toggle"></i></span> </div>
<div class="r-panel-body">

    <div class="row">
        <div class="col-xs-12">
            <h3><?php echo e(ucwords($task->heading)); ?></h3>
        </div>
        <div class="col-xs-6">
            <label for=""><?php echo app('translator')->getFromJson('modules.tasks.assignTo'); ?></label><br>
            <?php if($task->user->image): ?>
                <img src="<?php echo e(asset('user-uploads/avatar/'.$task->user->image)); ?>" class="img-circle" width="30" alt="">
            <?php else: ?>
                <img src="<?php echo e(asset('default-profile-2.png')); ?>" class="img-circle" width="30" alt="">
            <?php endif; ?>

            <?php echo e(ucwords($task->user->name)); ?>

        </div>
        <div class="col-xs-6">
            <label for=""><?php echo app('translator')->getFromJson('app.dueDate'); ?></label><br>
            <span <?php if($task->due_date->isPast()): ?> class="text-danger" <?php endif; ?>><?php echo e($task->due_date->format('d M, Y')); ?></span>
        </div>
        <div class="col-xs-12 task-description">
            <?php echo e(ucfirst($task->description)); ?>

        </div>

        <?php if($user->can('add_tasks')): ?>
        <div class="col-xs-12 m-t-20 m-b-10">
            <a href="javascript:;" id="show-task-row" class="btn btn-xs btn-success btn-outline"><i class="fa fa-plus"></i> <?php echo app('translator')->getFromJson('app.add'); ?> <?php echo app('translator')->getFromJson('modules.tasks.subTask'); ?></a>
        </div>
        <?php endif; ?>

        <div class="col-xs-12 m-t-20">
            <ul class="list-group" id="sub-task-list">
                <?php $__currentLoopData = $task->subtasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subtask): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li class="list-group-item row">
                        <div class="col-xs-6">
                            <div class="checkbox checkbox-success checkbox-circle task-checkbox">
                                <input class="task-check" data-sub-task-id="<?php echo e($subtask->id); ?>" id="checkbox<?php echo e($subtask->id); ?>" type="checkbox"
                                       <?php if($subtask->status == 'complete'): ?> checked <?php endif; ?>>
                                <label for="checkbox<?php echo e($subtask->id); ?>">&nbsp;</label>

                                <a href="#" class="text-muted <?php if($user->can('edit_tasks')): ?> edit-sub-task" <?php endif; ?> data-name="title"  data-url="<?php echo e(route('member.sub-task.update', $subtask->id)); ?>" data-pk="<?php echo e($subtask->id); ?>" data-type="text" data-value="<?php echo e(ucfirst($subtask->title)); ?>"><?php echo e(ucfirst($subtask->title)); ?></a>
                            </div>
                        </div>
                            <div class="col-xs-5 text-right">
                                <a href="#"  data-type="combodate" data-name="due_date" data-url="<?php echo e(route('member.sub-task.update', $subtask->id)); ?>"  data-emptytext="<?php echo app('translator')->getFromJson('app.dueDate'); ?>" class="m-r-10 <?php if($user->can('edit_tasks')): ?> edit-sub-task-date <?php endif; ?>"  data-format="YYYY-MM-DD" data-viewformat="DD/MM/YYYY" data-template="D / MMM / YYYY" data-value="<?php if($subtask->due_date): ?><?php echo e($subtask->due_date->format('Y-m-d')); ?><?php endif; ?>" data-pk="<?php echo e($subtask->id); ?>" data-title="<?php echo app('translator')->getFromJson('app.dueDate'); ?>"><?php if($subtask->due_date): ?><?php echo e($subtask->due_date->format('d M, Y')); ?><?php endif; ?></a>
                            </div>
                        <?php if($user->can('delete_tasks')): ?>
                            <div class="col-xs-1">
                                <a href="javascript:;" data-sub-task-id="<?php echo e($subtask->id); ?>" class="btn btn-danger btn-xs delete-sub-task"><i class="fa fa-times"></i></a>
                            </div>
                        <?php endif; ?>
                    </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </ul>

            <div class="row b-all m-t-10 p-10"  id="new-sub-task" style="display: none">
                <div class="col-xs-11 ">
                    <a href="javascript:;" id="create-sub-task" data-name="title"  data-url="<?php echo e(route('admin.sub-task.store')); ?>" class="text-muted" data-type="text"></a>
                </div>

                <div class="col-xs-1 text-right">
                    <a href="javascript:;" id="cancel-sub-task" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a>
                </div>
            </div>

        </div>

        <div class="col-xs-12 m-t-15">
            <h5><?php echo app('translator')->getFromJson('modules.tasks.comment'); ?></h5>
        </div>

        <div class="col-xs-12" id="comment-container">
            <div id="comment-list">
                <?php $__empty_1 = true; $__currentLoopData = $task->comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <div class="row b-b m-b-5 font-12">
                        <div class="col-xs-8">
                            <?php echo e(ucfirst($comment->comment)); ?><br>
                            <?php if($comment->user_id == $user->id): ?>
                                <a href="javascript:;" data-comment-id="<?php echo e($comment->id); ?>" class="text-danger delete-task-comment"><?php echo app('translator')->getFromJson('app.delete'); ?></a>
                            <?php endif; ?>
                        </div>
                        <div class="col-xs-4 text-right">
                            <?php echo e(ucfirst($comment->created_at->diffForHumans())); ?>

                        </div>
                        <div class="col-xs-12 text-right m-t-5 m-b-5">
                            &mdash; <i><?php echo e(ucwords($comment->user->name)); ?></i>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <div class="col-xs-12">
                        <?php echo app('translator')->getFromJson('messages.noRecordFound'); ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="form-group" id="comment-box">
                <div class="col-xs-9">
                    <input type="text" name="comment" id="task-comment" class="form-control" placeholder="<?php echo app('translator')->getFromJson('modules.tasks.comment'); ?>">
                </div>
                <div class="col-xs-3 text-right">
                    <a href="javascript:;" id="submit-comment" class="btn btn-success"><i class="fa fa-send"></i> <?php echo app('translator')->getFromJson('app.submit'); ?></a>
                </div>
            </div>
        </div>

    </div>

</div>



<script src="<?php echo e(asset('plugins/bower_components/moment/moment.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js')); ?>"></script>
<script>

    $('#create-sub-task').editable({
        send: 'always',
        type: 'text',
        emptytext: 'Enter task',
        mode: 'inline',
        params: {
            task_id: '<?php echo e($task->id); ?>',
            '_token':  '<?php echo e(csrf_token()); ?>'
        },
        success: function(response) {
            if(response.status == 'success'){
                $('#sub-task-list').html(response.view);

                $('.edit-sub-task').editable({
                    type: 'text',
                    emptytext: 'Enter task',
                    mode: 'inline',
                    validate: function(value) {
                        if ($.trim(value) == '') return 'This field is required';
                    }
                });

                $('.edit-sub-task-date').editable({
                    mode: 'inline',
                    combodate: {
                        minYear: '<?php echo e(\Carbon\Carbon::now()->subYear(20)->year); ?>',
                        maxYear: '<?php echo e(\Carbon\Carbon::now()->year); ?>'
                    }
                });

                $('#new-sub-task').hide();
            }
        },
        validate: function(value) {
            if ($.trim(value) == '') return 'This field is required';
        }
    });

    $('.edit-sub-task').editable({
        send: 'always',
        type: 'text',
        emptytext: 'Enter task',
        mode: 'inline',
        params: {
            task_id: '<?php echo e($task->id); ?>',
            '_method': 'PUT',
            '_token':  '<?php echo e(csrf_token()); ?>'
        },
        success: function(response) {
            if(response.status == 'success'){
                $('#sub-task-list').html(response.view);

                reinitializeList();
            }
        },
        validate: function(value) {
            if ($.trim(value) == '') return 'This field is required';
        }
    });

    $('.edit-sub-task-date').editable({
        send: 'always',
        type: 'text',
        emptytext: 'Enter task',
        mode: 'inline',
        combodate: {
            minYear: '<?php echo e(\Carbon\Carbon::now()->subYear(20)->year); ?>',
            maxYear: '<?php echo e(\Carbon\Carbon::now()->year); ?>'
        },
        params: {
            task_id: '<?php echo e($task->id); ?>',
            '_method': 'PUT',
            '_token':  '<?php echo e(csrf_token()); ?>'
        },
        success: function(response) {
            if(response.status == 'success'){
                $('#sub-task-list').html(response.view);

                reinitializeList();
            }
        },
        validate: function(value) {
            if ($.trim(value) == '') return 'This field is required';
        }
    });

    function reinitializeList() {
        $('.edit-sub-task').editable({
            type: 'text',
            emptytext: 'Enter task',
            mode: 'inline',
            validate: function(value) {
                if ($.trim(value) == '') return 'This field is required';
            },
            params: {
                task_id: '<?php echo e($task->id); ?>',
                '_method': 'PUT',
                '_token':  '<?php echo e(csrf_token()); ?>'
            }
        });

        $('.edit-sub-task-date').editable({
            mode: 'inline',
            combodate: {
                minYear: '<?php echo e(\Carbon\Carbon::now()->subYear(20)->year); ?>',
                maxYear: '<?php echo e(\Carbon\Carbon::now()->year); ?>'
            },
            params: {
                task_id: '<?php echo e($task->id); ?>',
                '_method': 'PUT',
                '_token':  '<?php echo e(csrf_token()); ?>'
            }
        });

        $('#new-sub-task').hide();
    }

    $('#show-task-row').click(function () {
        $('#new-sub-task').show();
    })

    $('#cancel-sub-task').click(function () {
        $('#new-sub-task').hide();
    })

    $('body').on('click', '.delete-sub-task', function () {
        var id = $(this).data('sub-task-id');
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the deleted sub task!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {

                var url = "<?php echo e(route('member.sub-task.destroy',':id')); ?>";
                url = url.replace(':id', id);

                var token = "<?php echo e(csrf_token()); ?>";

                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: {'_token': token, '_method': 'DELETE'},
                    success: function (response) {
                        if (response.status == "success") {
                            $('#sub-task-list').html(response.view);
                            reinitializeList();
                        }
                    }
                });
            }
        });
    });

    //    change sub task status
    $('#sub-task-list').on('click', '.task-check', function () {
        if ($(this).is(':checked')) {
            var status = 'complete';
        }else{
            var status = 'incomplete';
        }

        var id = $(this).data('sub-task-id');
        var url = "<?php echo e(route('member.sub-task.changeStatus')); ?>";
        var token = "<?php echo e(csrf_token()); ?>";

        $.easyAjax({
            url: url,
            type: "POST",
            data: {'_token': token, subTaskId: id, status: status},
            success: function (response) {
                if (response.status == "success") {
                    $('#sub-task-list').html(response.view);
                    reinitializeList();
                }
            }
        })
    });

    $('#submit-comment').click(function () {
        var comment = $('#task-comment').val();
        var token = '<?php echo e(csrf_token()); ?>';
        $.easyAjax({
            url: '<?php echo e(route("member.task-comment.store")); ?>',
            type: "POST",
            data: {'_token': token, comment: comment, taskId: '<?php echo e($task->id); ?>'},
            success: function (response) {
                if (response.status == "success") {
                    $('#comment-list').html(response.view);
                    $('#task-comment').val('');
                }
            }
        })
    })

    $('body').on('click', '.delete-task-comment', function () {
        var commentId = $(this).data('comment-id');
        var token = '<?php echo e(csrf_token()); ?>';

        var url = '<?php echo e(route("member.task-comment.destroy", ':id')); ?>';
        url = url.replace(':id', commentId);

        $.easyAjax({
            url: url,
            type: "POST",
            data: {'_token': token, '_method': 'DELETE', commentId: commentId},
            success: function (response) {
                if (response.status == "success") {
                    $('#comment-list').html(response.view);
                }
            }
        })
    })


</script>