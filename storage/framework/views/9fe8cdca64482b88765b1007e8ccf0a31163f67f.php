<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">

                <div class="col-md-1 col-xs-3">
                    <?php echo ($row->image) ? '<img src="'.asset('user-uploads/avatar/'.$row->image).'" alt="user" class="img-circle" width="40">' : '<img src="'.asset('default-profile-2.png').'" alt="user" class="img-circle" width="40">'; ?>

                </div>
                <div class="col-md-11 col-xs-9">
                    <?php echo e(ucwords($row->name)); ?> <br>
                    <span class="font-light text-muted"><?php echo e(ucfirst($row->job_title)); ?></span>
                </div>
                <div class="clearfix"></div>

            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    <div class="row">
                        <?php echo Form::open(['id'=>'attendance-container-'.$row->id,'class'=>'ajax-form','method'=>'POST']); ?>

                        <div class="form-body ">
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><?php echo app('translator')->getFromJson('modules.attendance.clock_in'); ?></label>
                                        <?php if(!is_null($row->clock_in_time)): ?> <span class="label label-success"><?php echo e(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->clock_in_time)->timezone($global->timezone)->format('h:i A')); ?> <i class="icon-check"></i></span> <?php else: ?>
                                            <label class="label label-danger"><?php echo app('translator')->getFromJson('modules.attendance.absent'); ?></label> <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" ><?php echo app('translator')->getFromJson('modules.attendance.late'); ?></label>
                                        <?php if($row->late == "yes"): ?> <span class="label label-success"><i class="fa fa-check"></i> <?php echo app('translator')->getFromJson('app.yes'); ?></span> <?php else: ?> -- <?php endif; ?>
                                    </div>
                                </div>

                            </div>

                            <div class="row m-t-15">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><?php echo app('translator')->getFromJson('modules.attendance.clock_out'); ?></label>
                                        <?php if(!is_null($row->clock_out_time)): ?> <span class="label label-success"><?php echo e(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->clock_out_time)->timezone($global->timezone)->format('h:i A')); ?> <i class="icon-check"></i></span> <?php else: ?> -- <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" ><?php echo app('translator')->getFromJson('modules.attendance.halfDay'); ?></label>
                                        <?php if($row->half_day == "yes"): ?> <span class="label label-success"><i class="fa fa-check"></i> <?php echo app('translator')->getFromJson('app.yes'); ?></span> <?php else: ?> -- <?php endif; ?>
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->getFromJson('modules.attendance.working_from'); ?></label>
                                        <?php echo e(isset($row->working_from) ? $row->working_from : '--'); ?>

                                    </div>
                                </div>



                            </div>

                        </div>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>