<?php for($date = $endDate; $date->diffInDays($startDate) > 0; $date->subDay()): ?>
    <?php
        $present = 0;
        $attendanceData = '';
    ?>

    <?php $__currentLoopData = $attendances; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $attendance): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if($attendance->clock_in_date == $date->toDateString()): ?>
            <?php
                $present = 1;
                $attendanceData = $attendance;
            ?>
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php if($present == 1): ?>
        <tr>
            <td><?php echo app('translator')->getFromJson('app.'.strtolower( $date->format("F") )); ?> <?php echo e($date->format('d, Y')); ?></td>
            <td><label class="label label-success"><?php echo app('translator')->getFromJson('modules.attendance.present'); ?></label></td>
            <td><?php echo e($attendanceData->clock_in_time->timezone($global->timezone)->format('h:i A')); ?></td>
            <td><?php if(!is_null($attendanceData->clock_out_time)): ?> <?php echo e($attendanceData->clock_out_time->timezone($global->timezone)->format('h:i A')); ?> <?php endif; ?></td>
            <td>
                <strong><?php echo app('translator')->getFromJson('modules.attendance.clock_in'); ?> IP: </strong> <?php echo e($attendanceData->clock_in_ip); ?><br>
                <strong><?php echo app('translator')->getFromJson('modules.attendance.clock_out'); ?> IP: </strong> <?php echo e($attendanceData->clock_out_ip); ?><br>
                <strong><?php echo app('translator')->getFromJson('modules.attendance.working_from'); ?>: </strong> <?php echo e($attendanceData->working_from); ?><br>
                <a href="javascript:;" data-attendance-id="<?php echo e($attendanceData->aId); ?>" class="delete-attendance btn btn-outline btn-danger btn-xs"><i class="fa fa-times"></i> <?php echo app('translator')->getFromJson('app.delete'); ?></a>
            </td>
        </tr>
    <?php else: ?>
        <tr>
            <td><?php echo app('translator')->getFromJson('app.'.strtolower( $date->format("F") )); ?> <?php echo e($date->format('d, Y')); ?></td>
            <td><label class="label label-danger"><?php echo app('translator')->getFromJson('modules.attendance.absent'); ?></label></td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
        </tr>
    <?php endif; ?>

<?php endfor; ?>

<?php
$present = 0;
$attendanceData = '';
$date = $endDate;
?>

<?php $__currentLoopData = $attendances; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $attendance): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php if($attendance->clock_in_date == $date->toDateString()): ?>
        <?php
        $present = 1;
        $attendanceData = $attendance;
        ?>
    <?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php if($present == 1): ?>
    <tr>
        <td><?php echo app('translator')->getFromJson('app.'.strtolower( $date->format("F") )); ?> <?php echo e($date->format('d, Y')); ?></td>
        <td><label class="label label-success"><?php echo app('translator')->getFromJson('modules.attendance.present'); ?></label></td>
        <td><?php echo e($attendanceData->clock_in_time->timezone($global->timezone)->format('h:i A')); ?></td>
        <td><?php if(!is_null($attendanceData->clock_out_time)): ?> <?php echo e($attendanceData->clock_out_time->timezone($global->timezone)->format('h:i A')); ?> <?php endif; ?></td>
        <td>
            <strong><?php echo app('translator')->getFromJson('modules.attendance.clock_in'); ?> IP: </strong> <?php echo e($attendanceData->clock_in_ip); ?><br>
            <strong><?php echo app('translator')->getFromJson('modules.attendance.clock_out'); ?> IP: </strong> <?php echo e($attendanceData->clock_out_ip); ?><br>
            <strong><?php echo app('translator')->getFromJson('modules.attendance.working_from'); ?>: </strong> <?php echo e($attendanceData->working_from); ?><br>
            <a href="javascript:;" data-attendance-id="<?php echo e($attendanceData->aId); ?>" class="delete-attendance btn btn-outline btn-danger btn-xs"><i class="fa fa-times"></i> <?php echo app('translator')->getFromJson('app.delete'); ?></a>
        </td>
    </tr>
<?php else: ?>
    <tr>
        <td><?php echo app('translator')->getFromJson('app.'.strtolower( $date->format("F") )); ?> <?php echo e($date->format('d, Y')); ?></td>
        <td><label class="label label-danger"><?php echo app('translator')->getFromJson('modules.attendance.absent'); ?></label></td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
    </tr>
<?php endif; ?>
