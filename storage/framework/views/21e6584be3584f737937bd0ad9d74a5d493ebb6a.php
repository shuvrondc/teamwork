<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e($pageTitle); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('member.dashboard')); ?>"><?php echo app('translator')->getFromJson('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('member.credit-discredit')); ?>"><?php echo e($pageTitle); ?></a></li>
                <li class="active">Add Credit</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('head-script'); ?>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/summernote/dist/summernote.css')); ?>">
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
         <div class="col-md-8">
         </div>
         <div class="col-md-4">

            <div class="jq-toast-wrap top-right">
                 <div class="success_msg jq-toast-single jq-has-icon jq-icon-success" style="text-align: left; display: none;">
                      <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 3.1s ease-in;-o-transition: width 3.1s ease-in;transition: width 3.1s ease-in;background-color: #ff6849;"></span>
                      <?php if($errors->any()): ?>
                           <?php echo e($errors->first()); ?>

                      <?php endif; ?>
            </div>

         </div>

         </div>
         <div class="col-sm-6">
            <div class="form-group">
                <a href="/member/credit-discredit/add-cause" class="btn btn-success btn-sm">Add Cause <i class="fa fa-plus" aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="col-md-12">


            <div class="panel panel-inverse">
                <div class="panel-heading"> Add Credit</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                       <form method="GET" action="/member/credit-discredit/add" >
                            <input type="hidden" name="type" value="1">
                        <div class="form-body">

                            <div class="row">
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                         <label class="control-label">Select  Employee</label>
                                         <?php 
                                        $employee_id=DB::table('role_user')->where('role_id',2)->orderBy('user_id','ASC')->get();
                                         ?>
                                        <?php if(isset($employee_id)): ?>
                                            <?php if(count($employee_id)>0): ?>
                                        <select class="selectpicker form-control" name="user_id" id="category_id"
                                                data-style="form-control">
                                            <?php $__currentLoopData = $employee_id; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee_id): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                 <?php 
                                                 $users=DB::table('users')->where('id',$employee_id->user_id)->get();
                                                  ?>
                                                 <?php if(isset($users)): ?>
                                                      <?php if(count($users)>0): ?>
                                                <option value="<?php echo e($users[0]->id); ?>"><?php echo e($users[0]->name); ?></option>
                                           <?php endif; ?>
                                           <?php endif; ?>

                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                   <?php else: ?>

                                   <?php endif; ?>
                                   <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 ">
                                    <div class="form-group">
                                        <label class="control-label">Select  Cause</label>
                                        <?php 
                                       $causes=DB::table('credit_discredit_cause')->where('type',1)->get();
                                        ?>
                                       <select class="selectpicker form-control" name="cause" id="category_id"
                                               data-style="form-control">
                                               <?php if(isset($causes)): ?>
                                                    <?php if(count($causes)>0): ?>
                                           <?php $__currentLoopData = $causes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cause): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                               <option value="<?php echo e($cause->id); ?>"><?php echo e($cause->cause_details); ?></option>

                                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                      <?php else: ?>

                                         
                                      <?php endif; ?>
                                 <?php endif; ?>
                                       </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">



                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Add Point</label>
                                        <input type="number" value="1" min="1" name="point" required class="form-control">
                                    </div>
                                </div>
                                <!--/span-->


                                <!--/span-->
                            </div>
                            <!--/row-->


                        </div>
                        <div class="form-actions">
                            <button type="submit" id="save-form" class="btn btn-success"><i class="fa fa-check"></i>
                                Add
                            </button>
                        </div>
                   </form>
                    </div>
                </div>
            </div>
            <div class="panel panel-inverse">
                <div class="panel-heading"> Recently Added Credits</div>

            <div class="white-box">

                  <?php 
                  $causes=DB::table('employee_credit_discredit')->where('type',1)->take(5)->orderBy('created_at','Desc')->get();
                   ?>
               <?php if(isset($causes)): ?>
               <?php if(count($causes)>0): ?>


                <div class="table-responsive">
                    <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="users-table">
                        <thead>
                        <tr>
                           <th>User Name</th>
                           <th>Credit</th>
                           <th>Point</th>
                           <th>Given By</th>
                           <th>Cause</th>
                           <th>Date</th>
                           <th><?php echo app('translator')->getFromJson('app.action'); ?></th>
                        </tr>
                        </thead>
                        <?php $__currentLoopData = $causes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cause): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <tr>
                             <td>
                                  <?php 
                                  $employee=DB::table('users')->where('id',$cause->user_id)->get();
                                   ?>
                                   <?php if(isset($employee)): ?>
                                       <?php if(count($employee)>0): ?><?php echo e($employee[0]->name); ?><?php endif; ?> <?php endif; ?>
                             </td>
                             <td><?php if($cause->type==2): ?><label class="label label-danger">Discredit</label><?php elseif($cause->type==1): ?><label class="label label-success">Credit</label><?php endif; ?></td>
                              <td><?php echo e($cause->point); ?></td>
                              <td><?php echo e($cause->given_by); ?></td>
                              <td>
                                   <?php 
                                   $cause_text=DB::table('credit_discredit_cause')->where('id',$cause->cause)->get();
                                    ?>
                                   <?php if(isset($cause_text)): ?>
                                       <?php if(count($cause_text)>0): ?><?php echo e($cause_text[0]->cause_details); ?><?php endif; ?> <?php endif; ?>
                                   </td>
                              <td>
                                   <?php  $date = new DateTime($cause->created_at);$date = $date->format('d-m-Y'); ?><?php echo e($date); ?>

                              </td>
                              <td>
                                   <?php if($user->can('edit_credit_discredit')): ?>
                                        <a href="/member/credit-discredit/edit/<?php echo e($cause->id); ?>" class="btn btn-info btn-circle" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                   <?php endif; ?>
                                   <?php if($user->can('delete_credit_discredit')): ?>
                      <a href="/member/credit-discredit/delete/<?php echo e($cause->id); ?>" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn-circle sa-params" data-toggle="tooltip" data-user-id="1" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                 <?php endif; ?>
            </td>

                        </tr>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </table>
                </div>
               <?php endif; ?>
               <?php endif; ?>
               </div>
          </div>
          </div>
        </div>
    </div>    <!-- .row -->

  <?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/summernote/dist/summernote.min.js')); ?>"></script>
<?php if($errors->any()): ?>
<script>
$('.success_msg').css("display", "block");;
      $('.success_msg').delay(2000).fadeOut('slow');
                setTimeout(function() {
                     $(".success_msg").modal('hide');
      }, 2500);
      </script>
<?php endif; ?>

<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.member-app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>