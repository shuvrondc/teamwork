<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e($pageTitle); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('member.dashboard')); ?>"><?php echo app('translator')->getFromJson('app.menu.home'); ?></a></li>
                <li class="active"><?php echo e($pageTitle); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
    <style>
        .col-in {
            padding: 0 20px !important;

        }

        .fc-event{
            font-size: 10px !important;
        }

    </style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="white-box">
                <div class="row row-in">
                    <div class="col-lg-3 col-sm-6 row-in-br">
                        <div class="col-in row">
                            <h3 class="box-title"><?php echo app('translator')->getFromJson('modules.dashboard.totalProjects'); ?></h3>
                            <ul class="list-inline two-part">
                                <li><i class="icon-layers text-info"></i></li>
                                <li class="text-right"><span class="counter"><?php echo e($totalProjects); ?></span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 row-in-br  b-r-none">
                        <div class="col-in row">
                            <h3 class="box-title"><?php echo app('translator')->getFromJson('modules.dashboard.totalHoursLogged'); ?></h3>
                            <ul class="list-inline two-part">
                                <li><i class="icon-clock text-warning"></i></li>
                                <li class="text-right"><span class="counter"><?php echo e($counts->totalHoursLogged); ?></span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6  row-in-br">
                        <div class="col-in row">
                            <h3 class="box-title"><?php echo app('translator')->getFromJson('modules.dashboard.totalPendingTasks'); ?></h3>
                            <ul class="list-inline two-part">
                                <li><i class="ti-alert text-danger"></i></li>
                                <li class="text-right"><span class="counter"><?php echo e($counts->totalPendingTasks); ?></span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 b-0">
                        <div class="col-in row">
                            <h3 class="box-title"><?php echo app('translator')->getFromJson('modules.dashboard.totalCompletedTasks'); ?></h3>
                            <ul class="list-inline two-part">
                                <li><i class="ti-check-box text-success"></i></li>
                                <li class="text-right"><span class="counter"><?php echo e($counts->totalCompletedTasks); ?></span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

    <div class="row">

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo app('translator')->getFromJson('app.menu.attendance'); ?></div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="col-xs-6">
                            <h3><?php echo app('translator')->getFromJson('modules.attendance.clock_in'); ?></h3>
                        </div>
                        <div class="col-xs-6">
                            <h3><?php echo app('translator')->getFromJson('modules.attendance.clock_in'); ?> IP</h3>
                        </div>
                        <div class="col-xs-6">
                            <?php if(is_null($todayAttendance)): ?>
                                <?php echo e(\Carbon\Carbon::now()->timezone($global->timezone)->format('h:i A')); ?>

                            <?php else: ?>
                                <?php echo e($todayAttendance->clock_in_time->timezone($global->timezone)->format('h:i A')); ?>

                            <?php endif; ?>
                        </div>
                        <div class="col-xs-6">
                            <?php echo e(isset($todayAttendance->clock_in_ip) ? $todayAttendance->clock_in_ip : request()->ip()); ?>

                        </div>

                        <?php if(!is_null($todayAttendance) && !is_null($todayAttendance->clock_out_time)): ?>
                            <div class="col-xs-6 m-t-20">
                                <label for=""><?php echo app('translator')->getFromJson('modules.attendance.clock_out'); ?></label>
                                <br><?php echo e($todayAttendance->clock_out_time->timezone($global->timezone)->format('h:i A')); ?>

                            </div>
                            <div class="col-xs-6 m-t-20">
                                <label for=""><?php echo app('translator')->getFromJson('modules.attendance.clock_out'); ?> IP</label>
                                <br><?php echo e($todayAttendance->clock_out_ip); ?>

                            </div>
                        <?php endif; ?>

                        <div class="col-xs-12 m-t-20">
                            <label for=""><?php echo app('translator')->getFromJson('modules.attendance.working_from'); ?></label>
                            <?php if(is_null($todayAttendance)): ?>
                                <input type="text" class="form-control" id="working_from" name="working_from">
                            <?php else: ?>
                                <br> <?php echo e($todayAttendance->working_from); ?>

                            <?php endif; ?>
                        </div>

                        <div class="col-xs-6 m-t-20">
                            <?php if(is_null($todayAttendance)): ?>
                                <button class="btn btn-success btn-sm" id="clock-in"><?php echo app('translator')->getFromJson('modules.attendance.clock_in'); ?></button>
                            <?php endif; ?>
                            <?php if(!is_null($todayAttendance) && is_null($todayAttendance->clock_out_time)): ?>
                                <button class="btn btn-danger btn-sm" id="clock-out"><?php echo app('translator')->getFromJson('modules.attendance.clock_out'); ?></button>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo app('translator')->getFromJson('modules.dashboard.overdueTasks'); ?></div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <ul class="list-task list-group" data-role="tasklist">
                            <li class="list-group-item" data-role="task">
                                <strong><?php echo app('translator')->getFromJson('app.title'); ?></strong> <span
                                        class="pull-right"><strong><?php echo app('translator')->getFromJson('app.dueDate'); ?></strong></span>
                            </li>
                            <?php $__empty_1 = true; $__currentLoopData = $pendingTasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$task): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                <li class="list-group-item" data-role="task">
                                    <?php echo e(($key+1).'. '.ucfirst($task->heading)); ?>

                                    <?php if(!is_null($task->project_id)): ?>
                                        <a href="<?php echo e(route('member.projects.show', $task->project_id)); ?>" class="text-danger"><?php echo e(ucwords($task->project->project_name)); ?></a>
                                    <?php endif; ?>
                                    <label class="label label-danger pull-right"><?php echo e($task->due_date->format('d M')); ?></label>
                                </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                <li class="list-group-item" data-role="task">
                                    <?php echo app('translator')->getFromJson('messages.noOpenTasks'); ?>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row" >

        <div class="col-md-6" id="project-timeline">
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo app('translator')->getFromJson('modules.dashboard.projectActivityTimeline'); ?></div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="steamline">
                            <?php $__currentLoopData = $projectActivities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $activity): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="sl-item">
                                    <div class="sl-left"><i class="fa fa-circle text-info"></i>
                                    </div>
                                    <div class="sl-right">
                                        <div><h6><a href="<?php echo e(route('member.projects.show', $activity->project_id)); ?>" class="text-danger"><?php echo e(ucwords($activity->project_name)); ?>:</a> <?php echo e($activity->activity); ?></h6> <span class="sl-date"><?php echo e($activity->created_at->timezone($global->timezone)->diffForHumans()); ?></span></div>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo app('translator')->getFromJson('modules.dashboard.userActivityTimeline'); ?></div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="steamline">
                            <?php $__empty_1 = true; $__currentLoopData = $userActivities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$activity): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                <div class="sl-item">
                                    <div class="sl-left">
                                        <?php echo ($activity->user->image) ? '<img src="'.asset('user-uploads/avatar/'.$activity->user->image).'"
                                                                    alt="user" class="img-circle">' : '<img src="'.asset('default-profile-2.png').'"
                                                                    alt="user" class="img-circle">'; ?>

                                    </div>
                                    <div class="sl-right">
                                        <div class="m-l-40"><a href="<?php echo e(route('member.employees.show', $activity->user_id)); ?>" class="text-success"><?php echo e(ucwords($activity->user->name)); ?></a> <span  class="sl-date"><?php echo e($activity->created_at->timezone($global->timezone)->diffForHumans()); ?></span>
                                            <p><?php echo ucfirst($activity->activity); ?></p>
                                        </div>
                                    </div>
                                </div>
                                <?php if(count($userActivities) > ($key+1)): ?>
                                    <hr>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                <div><?php echo app('translator')->getFromJson('messages.noActivityByThisUser'); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script>
    $('#clock-in').click(function () {
        var workingFrom = $('#working_from').val();

        var token = "<?php echo e(csrf_token()); ?>";

        $.easyAjax({
            url: '<?php echo e(route('member.attendances.store')); ?>',
            type: "POST",
            data: {
                working_from: workingFrom,
                _token: token
            },
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    })

    <?php if(!is_null($todayAttendance)): ?>
    $('#clock-out').click(function () {

        var token = "<?php echo e(csrf_token()); ?>";

        $.easyAjax({
            url: '<?php echo e(route('member.attendances.update', $todayAttendance->id)); ?>',
            type: "PUT",
            data: {
                _token: token
            },
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    })
    <?php endif; ?>

</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.member-app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>