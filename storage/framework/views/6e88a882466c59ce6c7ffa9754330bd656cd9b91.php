<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">

                <div class="col-md-1 col-xs-3">
                    <?php echo ($row->image) ? '<img src="'.asset('user-uploads/avatar/'.$row->image).'" alt="user" class="img-circle" width="40">' : '<img src="'.asset('default-profile-2.png').'" alt="user" class="img-circle" width="40">'; ?>

                </div>
                <div class="col-md-9 col-xs-7">
                    <?php echo e(ucwords($row->name)); ?> <br>
                    <span class="font-light text-muted"><?php echo e(ucfirst($row->job_title)); ?></span>
                </div>
                <div class="col-md-2 col-xs-2">
                    <?php if(!is_null($row->clock_in_time)): ?>
                        <label class="label label-success"><i class="fa fa-check"></i> <?php echo app('translator')->getFromJson('modules.attendance.present'); ?></label>
                    <?php else: ?>
                        <label class="label label-danger"><i class="fa fa-exclamation-circle"></i> <?php echo app('translator')->getFromJson('modules.attendance.absent'); ?></label>
                    <?php endif; ?>
                </div>
                <div class="clearfix"></div>

            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    <div class="row">
                        <?php echo Form::open(['id'=>'attendance-container-'.$row->id,'class'=>'ajax-form','method'=>'POST']); ?>

                        <div class="form-body ">
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <label><?php echo app('translator')->getFromJson('modules.attendance.clock_in'); ?></label>
                                        <input type="text" name="clock_in_time"
                                               class="form-control a-timepicker" id="clock-in-<?php echo e($row->id); ?>"
                                               <?php if(!is_null($row->clock_in_time)): ?> value="<?php echo e(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->clock_in_time)->timezone($global->timezone)->format('h:i A')); ?>" <?php endif; ?>>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->getFromJson('modules.attendance.clock_in'); ?> IP</label>
                                        <input type="text" name="clock_in_ip" id="clock-in-ip-<?php echo e($row->id); ?>"
                                               class="form-control" value="<?php echo e(isset($row->clock_in_ip) ? $row->clock_in_ip : request()->ip()); ?>">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" ><?php echo app('translator')->getFromJson('modules.attendance.late'); ?></label>
                                        <div class="switchery-demo">
                                            <input type="checkbox" <?php if($row->late == "yes"): ?> checked <?php endif; ?> class="js-switch change-module-setting" data-color="#ed4040" id="late-<?php echo e($row->id); ?>"  />
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row m-t-15">

                                <div class="col-md-4">
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <label><?php echo app('translator')->getFromJson('modules.attendance.clock_out'); ?></label>
                                        <input type="text" name="clock_out_time" id="clock-out-<?php echo e($row->id); ?>"
                                               class="form-control b-timepicker"
                                               <?php if(!is_null($row->clock_out_time)): ?> value="<?php echo e(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->clock_out_time)->timezone($global->timezone)->format('h:i A')); ?>" <?php endif; ?>>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->getFromJson('modules.attendance.clock_out'); ?> IP</label>
                                        <input type="text" name="clock_out_ip" id="clock-out-ip-<?php echo e($row->id); ?>"
                                               class="form-control" value="<?php echo e(isset($row->clock_out_ip) ? $row->clock_out_ip : request()->ip()); ?>">
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" ><?php echo app('translator')->getFromJson('modules.attendance.halfDay'); ?></label>
                                            <div class="switchery-demo">
                                                <input type="checkbox" <?php if($row->half_day == "yes"): ?> checked <?php endif; ?> class="js-switch change-module-setting" data-color="#ed4040" id="halfday-<?php echo e($row->id); ?>"  />
                                            </div>
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->getFromJson('modules.attendance.working_from'); ?></label>
                                        <input type="text" name="working_from" id="working-from-<?php echo e($row->id); ?>"
                                               class="form-control" value="<?php echo e(isset($row->working_from) ? $row->working_from : 'office'); ?>">
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-success text-white save-attendance"
                                                data-user-id="<?php echo e($row->id); ?>"><i
                                                    class="fa fa-check"></i> <?php echo app('translator')->getFromJson('app.save'); ?></button>
                                    </div>
                                </div>


                            </div>

                        </div>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>