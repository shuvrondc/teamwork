<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e($pageTitle); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->getFromJson('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.settings.index')); ?>"><?php echo app('translator')->getFromJson('app.menu.settings'); ?></a></li>
                <li class="active"><?php echo e($pageTitle); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading"><?php echo app('translator')->getFromJson('app.menu.replyTemplates'); ?></div>

                <div class="vtabs customvtab m-t-10">

                    <?php echo $__env->make('sections.ticket_setting_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                    <div class="tab-content">
                        <div id="vhome3" class="tab-pane active">

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="white-box">
                                        <h3><?php echo app('translator')->getFromJson('app.addNew'); ?> <?php echo app('translator')->getFromJson('modules.tickets.template'); ?></h3>

                                        <?php echo Form::open(['id'=>'createTemplate','class'=>'ajax-form','method'=>'POST']); ?>


                                        <div class="form-body">

                                            <div class="form-group">
                                                <label><?php echo app('translator')->getFromJson('modules.tickets.templateHeading'); ?></label>
                                                <input type="text" name="reply_heading" id="reply_heading" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label><?php echo app('translator')->getFromJson('modules.tickets.templateText'); ?></label>
                                                <textarea name="reply_text" id="reply_text" class="form-control" rows="10"></textarea>
                                            </div>

                                            <div class="form-actions">
                                                <button type="submit" id="save-template" class="btn btn-success"><i
                                                            class="fa fa-check"></i> <?php echo app('translator')->getFromJson('app.save'); ?>
                                                </button>
                                            </div>
                                        </div>

                                        <?php echo Form::close(); ?>


                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="white-box">
                                        <h3><?php echo app('translator')->getFromJson('app.menu.replyTemplates'); ?></h3>


                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th><?php echo app('translator')->getFromJson('modules.tickets.templateHeading'); ?></th>
                                                    <th><?php echo app('translator')->getFromJson('app.action'); ?></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php $__empty_1 = true; $__currentLoopData = $templates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$template): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                                    <tr>
                                                        <td><?php echo e(($key+1)); ?></td>
                                                        <td><?php echo e(ucwords($template->reply_heading)); ?></td>
                                                        <td>
                                                            <a href="javascript:;" data-template-id="<?php echo e($template->id); ?>"
                                                               class="btn btn-sm btn-info btn-rounded btn-outline edit-template"><i
                                                                        class="fa fa-edit"></i> <?php echo app('translator')->getFromJson('app.edit'); ?></a>
                                                            <a href="javascript:;" data-template-id="<?php echo e($template->id); ?>"
                                                               class="btn btn-sm btn-danger btn-rounded btn-outline delete-template"><i
                                                                        class="fa fa-times"></i> <?php echo app('translator')->getFromJson('app.remove'); ?></a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo app('translator')->getFromJson('messages.noTemplateFound'); ?>
                                                        </td>
                                                    </tr>
                                                <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>
    <!-- .row -->


    
    <div class="modal fade bs-modal-md in" id="ticketTemplateModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script type="text/javascript">


    //    save project members
    $('#save-template').click(function () {
        $.easyAjax({
            url: '<?php echo e(route('admin.replyTemplates.store')); ?>',
            container: '#createTemplate',
            type: "POST",
            data: $('#createTemplate').serialize(),
            success: function (response) {
                if (response.status == "success") {
                    $.unblockUI();
                    window.location.reload();
                }
            }
        })
    });


    $('body').on('click', '.delete-template', function () {
        var id = $(this).data('template-id');
        swal({
            title: "Are you sure?",
            text: "This will remove the template from the list.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {

                var url = "<?php echo e(route('admin.replyTemplates.destroy',':id')); ?>";
                url = url.replace(':id', id);

                var token = "<?php echo e(csrf_token()); ?>";

                $.easyAjax({
                    type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                    success: function (response) {
                        if (response.status == "success") {
                            $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                            window.location.reload();
                        }
                    }
                });
            }
        });
    });


    $('.edit-template').click(function () {
        var typeId = $(this).data('template-id');
        var url = '<?php echo e(route("admin.replyTemplates.edit", ":id")); ?>';
        url = url.replace(':id', typeId);

        $('#modelHeading').html("<?php echo e(__('app.edit')." ".__('app.menu.replyTemplates')); ?>");
        $.ajaxModal('#ticketTemplateModal', url);
    })


</script>


<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>