<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Leave extends Model
{
    protected $dates = ['leave_date'];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function type(){
        return $this->belongsTo(LeaveType::class, 'leave_type_id');
    }

    public static function byUser($userId){
        $setting = Setting::first();
        $user = User::find($userId);

        if($setting->leaves_start_from == 'joining_date'){
            return Leave::where('user_id', $userId)
                ->where('leave_date','>=', $user->employee[0]->joining_date->format('Y-m-d'))
                ->where('leave_date','<', $user->employee[0]->joining_date->addYear(1)->format('Y-m-d'))
                ->where('status', 'approved')
                ->get();
        }
        else{
            return Leave::where('user_id', $userId)
                ->where('leave_date','>=', Carbon::today()->startOfYear()->format('Y-m-d'))
                ->where('leave_date','<', Carbon::today()->endOfYear()->format('Y-m-d'))
                ->where('status', 'approved')
                ->get();
        }
    }
}
