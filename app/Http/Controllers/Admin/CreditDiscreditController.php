<?php

namespace App\Http\Controllers\Admin;

use App\EmployeeDetails;
use App\Helper\Reply;
use App\Http\Requests\User\StoreUser;
use App\Http\Requests\User\UpdateEmployee;
use App\Leave;
use App\LeaveType;
use App\Notifications\NewUser;
use App\Project;
use App\ProjectTimeLog;
use App\Role;
use App\RoleUser;
use App\Task;
use App\User;
use App\UserActivity;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Facades\Datatables;
Use DB;
Use Auth;
use Illuminate\Support\Facades\Redirect;

class CreditDiscreditController extends AdminBaseController
{

    public function __construct() {
        parent::__construct();
        $this->pageTitle =__('Credit-Discredit');
        $this->pageIcon = 'icon-user';
    }
    public function index(Request $req) {
         if(isset($req->starting_date)){
              $this->starting_date=$req->starting_date;
         }
         if(isset($req->user_id)){
              if($req->user_id!=''){
              $this->user_id=$req->user_id;

         }
         }
         if(isset($req->ending_date)){
              $this->ending_date=$req->ending_date;
         }

        return view('admin.credit-discredit.index', $this->data);
    }
    public function addcauseview() {
        return view('admin.credit-discredit.add-cause', $this->data);
    }
    public function createcause(Request $req) {

         $add_credit=DB::table('credit_discredit_cause')->insert(array(
              'type'=>$req->type,
              'cause_details'=>$req->cause_details,

         ));

         return back()->withErrors('Added Successfully.');
    }
    public function deletecause($id) {

         $delete=DB::table('credit_discredit_cause')->where('id',$id)->delete();

         return back()->withErrors('Deleted Successfully.');
    }
    public function editcauseview($id) {
         $this->id=$id;

      return view('admin.credit-discredit.editcause', $this->data);
    }
    public function updatecause(Request $req) {

         $up_cause=DB::table('credit_discredit_cause')->where('id',$req->id)->update(array(
              'type'=>$req->type,
              'cause_details'=>$req->cause_details,

         ));

         return Redirect::route('admin.credit-discredit-addcause')->withErrors('Update Successfully.');
    }
    public function addcreditview() {
        return view('admin.credit-discredit.add-credit', $this->data);
    }
    public function adddiscreditview() {
        return view('admin.credit-discredit.add-credit', $this->data);
    }
    public function delete($id) {

         $delete=DB::table('employee_credit_discredit')->where('id',$id)->delete();

        return back()->withErrors('Deleted Successfully.');
    }
    public function addcredit(Request $req) {

         $add_credit=DB::table('employee_credit_discredit')->insert(array(
              'user_id'=>$req->user_id,
              'type'=>$req->type,
              'specific_cause'=>$req->specific_cause,
              'point'=>$req->point,
              'cause'=>$req->cause,
              'given_by'=>Auth::User()->name,

         ));

        return back()->withErrors('Added Successfully.');
    }
    public function update(Request $req) {

         $add_credit=DB::table('employee_credit_discredit')->where('id',$req->id)->update(array(
              'user_id'=>$req->user_id,
              'type'=>$req->type,
              'specific_cause'=>$req->specific_cause,
              'point'=>$req->point,
              'cause'=>$req->cause,
              'given_by'=>Auth::User()->name,

         ));
         if($req->type==1){
         return Redirect::route('admin.add-credit-view')->withErrors('Update Successfully.');
    }elseif($req->type==2){

        return Redirect::route('admin.add-discredit-view')->withErrors('Update Successfully');
   }else{
        return back();
   }
    }
    public function editcreditdiscreditshow($id) {
         $this->id=$id;

      return view('admin.credit-discredit.edit', $this->data);
    }
    public function adddiscredit() {
        return view('admin.credit-discredit.add-discredit', $this->data);
    }
    public function viewcauses(Request $req,$id) {
         $this->id=$id;
         if(isset($req->starting_date)){
              $this->starting_date=$req->starting_date;
         }
         if(isset($req->ending_date)){
              $this->ending_date=$req->ending_date;
         }
        return view('admin.credit-discredit.view-causes', $this->data);
    }




}
