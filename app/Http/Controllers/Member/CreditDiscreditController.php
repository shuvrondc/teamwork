<?php

namespace App\Http\Controllers\Member;

use App\Currency;
use App\Estimate;
use App\Helper\Reply;
use App\Http\Requests\Invoices\StoreInvoice;
use App\Invoice;
use App\InvoiceItems;
use App\InvoiceSetting;
use App\Notifications\NewInvoice;
use App\Project;
use App\Setting;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
Use DB;
Use Auth;
use Illuminate\Support\Facades\Redirect;

class CreditDiscreditController extends MemberBaseController
{

    public function __construct() {
        parent::__construct();
        $this->pageTitle =__('Credit-Discredit');
        $this->pageIcon = 'icon-user';
    }
    public function index(Request $req) {
         if(isset($req->starting_date)){
              $this->starting_date=$req->starting_date;
         }
         if(isset($req->ending_date)){
              $this->ending_date=$req->ending_date;
         }


        return view('member.credit-discredit.index', $this->data);
    }
    public function viewall(Request $req) {
         if(Auth::User()){
         $role=DB::table('role_user')->where('user_id',Auth::User()->id)->orderBy('role_id','DESC')->get();
         $permission=DB::table('permission_role')->where('permission_id',48)->where('role_id',$role[0]->role_id)->get();

         if(count($permission)>0){
         if(isset($req->starting_date)){
              $this->starting_date=$req->starting_date;
         }
         if(isset($req->ending_date)){
              $this->ending_date=$req->ending_date;
         }
         if(isset($req->user_id)){
              if($req->user_id!=''){
              $this->user_id=$req->user_id;

         }
         }

        return view('member.credit-discredit.viewall', $this->data);
   }else{
        return view('errors.403');

   }
    }
    }
    public function addcreditview() {
         if(Auth::User()){
         $role=DB::table('role_user')->where('user_id',Auth::User()->id)->orderBy('role_id','DESC')->get();
         $permission=DB::table('permission_role')->where('permission_id',47)->where('role_id',$role[0]->role_id)->get();

         if(count($permission)>0){

              return view('member.credit-discredit.add-credit', $this->data);
  }else{
       return view('errors.403');

  }
    }
    }
    public function adddiscreditview() {
         if(Auth::User()){
         $role=DB::table('role_user')->where('user_id',Auth::User()->id)->orderBy('role_id','DESC')->get();
         $permission=DB::table('permission_role')->where('permission_id',48)->where('role_id',$role[0]->role_id)->get();

         if(count($permission)>0){

              return view('member.credit-discredit.add-credit', $this->data);
  }else{
       return view('errors.403');

  }
    }
    }
    public function delete($id) {
         if(Auth::User()){
         $role=DB::table('role_user')->where('user_id',Auth::User()->id)->orderBy('role_id','DESC')->get();
         $permission=DB::table('permission_role')->where('permission_id',50)->where('role_id',$role[0]->role_id)->get();

         if(count($permission)>0){

              $delete=DB::table('employee_credit_discredit')->where('id',$id)->delete();

              return back()->withErrors('Deleted Successfully.');
  }else{
       return view('errors.403');

  }
    }

    }
    public function addcredit(Request $req) {

         $add_credit=DB::table('employee_credit_discredit')->insert(array(
              'user_id'=>$req->user_id,
              'type'=>$req->type,
              'specific_cause'=>$req->specific_cause,
              'point'=>$req->point,
              'cause'=>$req->cause,
              'given_by'=>Auth::User()->name,

         ));

        return back()->withErrors('Added Successfully.');
    }
    public function update(Request $req) {
         if(Auth::User()){
         $role=DB::table('role_user')->where('user_id',Auth::User()->id)->orderBy('role_id','DESC')->get();
         $permission=DB::table('permission_role')->where('permission_id',49)->where('role_id',$role[0]->role_id)->get();

         if(count($permission)>0){
              $add_credit=DB::table('employee_credit_discredit')->where('id',$req->id)->update(array(
                   'user_id'=>$req->user_id,
                   'type'=>$req->type,
                   'specific_cause'=>$req->specific_cause,
                   'point'=>$req->point,
                   'cause'=>$req->cause,
                   'given_by'=>Auth::User()->name,

              ));
                   return Redirect::route('member.credit-discredit')->withErrors('Update Successfully.');


  }else{
       return view('errors.403');

  }
    }

    }
    public function editcreditdiscreditshow($id) {
         if(Auth::User()){
         $role=DB::table('role_user')->where('user_id',Auth::User()->id)->orderBy('role_id','DESC')->get();
         $permission=DB::table('permission_role')->where('permission_id',49)->where('role_id',$role[0]->role_id)->get();

         if(count($permission)>0){

              $this->id=$id;

              return view('member.credit-discredit.edit', $this->data);
  }else{
       return view('errors.403');

  }
    }
    }
    public function adddiscredit() {
         if(Auth::User()){
         $role=DB::table('role_user')->where('user_id',Auth::User()->id)->orderBy('role_id','DESC')->get();
         $permission=DB::table('permission_role')->where('permission_id',47)->where('role_id',$role[0]->role_id)->get();

         if(count($permission)>0){

              return view('member.credit-discredit.add-discredit', $this->data);
  }else{
       return view('errors.403');

  }
    }
    }
    public function viewcauses(Request $req,$id) {
         if(Auth::User()){
        $role=DB::table('role_user')->where('user_id',Auth::User()->id)->orderBy('role_id','DESC')->get();
        $permission=DB::table('permission_role')->where('permission_id',48)->where('role_id',$role[0]->role_id)->get();

        if(count($permission)>0){
              $this->id=$id;
              if(isset($req->starting_date)){
                   $this->starting_date=$req->starting_date;
              }
              if(isset($req->ending_date)){
                   $this->ending_date=$req->ending_date;
              }
              return view('member.credit-discredit.view-causes', $this->data);
         }else{
            return view('errors.403');

       }
          }
    }
    public function addcauseview() {
         if(Auth::User()){
       $role=DB::table('role_user')->where('user_id',Auth::User()->id)->orderBy('role_id','DESC')->get();
       $permission=DB::table('permission_role')->where('permission_id',47)->where('role_id',$role[0]->role_id)->get();

       if(count($permission)>0){
        return view('member.credit-discredit.add-cause', $this->data);
   }else{
      return view('errors.403');

}
    }
    }
    public function createcause(Request $req) {
         if(Auth::User()){
       $role=DB::table('role_user')->where('user_id',Auth::User()->id)->orderBy('role_id','DESC')->get();
       $permission=DB::table('permission_role')->where('permission_id',47)->where('role_id',$role[0]->role_id)->get();

       if(count($permission)>0){

         $add_credit=DB::table('credit_discredit_cause')->insert(array(
             'type'=>$req->type,
             'cause_details'=>$req->cause_details,

         ));

         return back()->withErrors('Added Successfully.');
    }else{
      return view('errors.403');

}
    }
    }
    public function deletecause($id) {
         if(Auth::User()){
      $role=DB::table('role_user')->where('user_id',Auth::User()->id)->orderBy('role_id','DESC')->get();
      $permission=DB::table('permission_role')->where('permission_id',47)->where('role_id',$role[0]->role_id)->get();

      if(count($permission)>0){

         $delete=DB::table('credit_discredit_cause')->where('id',$id)->delete();

         return back()->withErrors('Deleted Successfully.');
    }else{
      return view('errors.403');

}
    }
    }
    public function editcauseview($id) {
         if(Auth::User()){
      $role=DB::table('role_user')->where('user_id',Auth::User()->id)->orderBy('role_id','DESC')->get();
      $permission=DB::table('permission_role')->where('permission_id',47)->where('role_id',$role[0]->role_id)->get();

      if(count($permission)>0){
         $this->id=$id;

     return view('member.credit-discredit.editcause', $this->data);
}else{
   return view('errors.403');

}
}
    }
    public function updatecause(Request $req) {
         if(Auth::User()){
      $role=DB::table('role_user')->where('user_id',Auth::User()->id)->orderBy('role_id','DESC')->get();
      $permission=DB::table('permission_role')->where('permission_id',47)->where('role_id',$role[0]->role_id)->get();

      if(count($permission)>0){

         $up_cause=DB::table('credit_discredit_cause')->where('id',$req->id)->update(array(
             'type'=>$req->type,
             'cause_details'=>$req->cause_details,

         ));

         return Redirect::route('member.credit-discredit-addcause')->withErrors('Update Successfully.');
    }else{
      return view('errors.403');

}
    }
    }



}
