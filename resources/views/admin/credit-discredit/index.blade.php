@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ $pageTitle }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush

@section('content')

    <div class="row">
         <div class="col-md-6">
         </div>
         <div class="col-md-6">

           <div class="jq-toast-wrap top-right">
                <div class="success_msg jq-toast-single jq-has-icon jq-icon-success" style="text-align: left; display: none;">
                     <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 3.1s ease-in;-o-transition: width 3.1s ease-in;transition: width 3.1s ease-in;background-color: #ff6849;"></span>
                     Updated Successfully.
           </div>
       </div>
         </div>

         <div class="col-md-12">
              @php
              $employee_list=DB::table('role_user')->where('role_id',2)->orderBy('user_id','ASC')->get();
              @endphp
              @if(isset($employee_list))
              @if(count($employee_list)>0)

              <div class="white-box p-b-0 bg-inverse text-white">
                 <div class="row">
                      <form method="GET" action="/admin/credit-discredit" >

                          <div class="col-md-3">
                              <div class="form-group">
                                  <label class="control-label">Starting Date</label>
                                  <input type="text" class="form-control" name="starting_date" id="credit_discredit_starting_date" value="@if(isset($starting_date)){{ $starting_date }}@else{{ Carbon\Carbon::today()->format('Y-m-d') }}@endif" onchange="checkstart()">
                              </div>

                          </div>
                          <div class="col-md-3">
                              <div class="form-group">
                                  <label class="control-label">Ending Date</label>
                                  <input type="text" class="form-control" name="ending_date" id="credit_discredit_ending_date" value="@if(isset($ending_date)){{ $ending_date }}@else{{ Carbon\Carbon::today()->format('Y-m-d') }}@endif" onchange="checkend()">
                              </div>

                          </div>
                          <div class="col-md-3">
                              <label class="control-label">Employee Name</label>

                              <div class="form-group">
                                  <div class="row">
                                      <div class="col-md-12">
                                          <select name="user_id" class="select2 form-control" data-placeholder="@lang('modules.employees.employeeName')" id="employeeId" onchange="this.form.submit()">
                                              <option value="">View All</option>
                                              @foreach($employee_list as $e_id)
                                                   @php
                                                   $employeelist=DB::table('users')->where('id',$e_id->user_id)->get();
                                                   @endphp
                                                  <option
                                                          value="{{ $employeelist[0]->id }}">{{ ucwords($employeelist[0]->name) }}</option>
                                              @endforeach
                                          </select>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-3">
                               <div class="form-group m-t-25">
                                    <button type="submit" id="apply-filter" class="btn btn-success btn-block">Apply</button>
                               </div>
                          </div>
                     </form>



                 </div>

             </div>
        @endif
        @endif
             <div class="white-box">
                  @if (isset($user_id))
                       @php
                       $employee_id=DB::table('role_user')->where('role_id',2)->where('user_id',$user_id)->orderBy('user_id','ASC')->get();
                       @endphp

                  @else

                       @php
                       $employee_id=DB::table('role_user')->where('role_id',2)->orderBy('user_id','ASC')->get();
                       @endphp
                  @endif
                


                @if(isset($employee_id))
                @if(count($employee_id)>0)


                <div class="table-responsive">
                    <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="users-table">
                        <thead>
                        <tr>
                            <th>User Id</th>
                            <th>@lang('app.name')</th>
                            <th>Credit</th>
                            <th>Discredit</th>
                            <th>Difference</th>
                            <th>@lang('app.action')</th>
                        </tr>
                        </thead>
                        @foreach($employee_id as $employee_id)
                             @php
                             $credit_point=0;
                              $discredit_point=0;

                             @endphp

                                  @php

                                  $employee=DB::table('users')->where('id',$employee_id->user_id)->get();
                                  @endphp



                             @if(isset($employee))
                             @if(count($employee)>0)


                        <tr>
                             <td>{{ $employee[0]->id }}</td>
                             <td>{{ $employee[0]->name }}</td>
                            @if(isset($starting_date))

                           @if(isset($ending_date))
                                @php
                                $credit=DB::table('employee_credit_discredit')->where('user_id',$employee[0]->id)->where('type',1)->where('created_at','>=',$starting_date.' 00:00:00')->where('created_at','<=',$ending_date.' 23:59:59')->get();
                                @endphp
                           @else
                                @php
                                $credit=DB::table('employee_credit_discredit')->where('user_id',$employee[0]->id)->where('type',1)->get();
                                @endphp


                           @endif
                           @else
                                @php
                                $credit=DB::table('employee_credit_discredit')->where('user_id',$employee[0]->id)->where('type',1)->get();
                                @endphp
                           @endif
                           @if(isset($credit))
                           @if(count($credit)>0)
                            @foreach($credit as $credit)
                                 @php
                                 $credit_point=$credit_point+$credit->point;
                                 @endphp
                            @endforeach
                              @endif
                              @endif
                             <td>{{ $credit_point }}</td>

                             @if(isset($starting_date))

                          @if(isset($ending_date))
                               @php
                              $discredit=DB::table('employee_credit_discredit')->where('user_id',$employee[0]->id)->where('type',2)->where('created_at','>=',$starting_date.' 00:00:00')->where('created_at','<=',$ending_date.' 23:59:59')->get();
                               @endphp
                          @else
                               @php
                              $discredit=DB::table('employee_credit_discredit')->where('type',2)->where('user_id',$employee[0]->id)->get();
                               @endphp


                          @endif
                          @else
                               @php
                               $discredit=DB::table('employee_credit_discredit')->where('type',2)->where('user_id',$employee[0]->id)->get();
                               @endphp
                          @endif
                          @if(isset($discredit))
                          @if(count($discredit)>0)

                            @foreach($discredit as $discredit)
                                 @php
                                 $discredit_point=$discredit_point+$discredit->point;
                                 @endphp
                            @endforeach
                       @endif
                       @endif
                             <td>{{ $discredit_point }}</td>
                             <td>{{ $credit_point-$discredit_point }}</td>
                             <td><a href="/admin/credit-discredit-cause/{{ $employee[0]->id }}" class="btn btn-success btn-circle" data-toggle="tooltip" data-original-title="View Credit/Discredit Causes"><i class="fa fa-search" aria-hidden="true"></i></a></td>
                        </tr>
                   @endif
                   @endif
                         @endforeach

                    </table>
                </div>
           @endif
           @endif
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script>
jQuery('#credit_discredit_starting_date').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd',
    todayHighlight: true,
    endDate: '+0d'
}).on('changeDate', function (ev) {
    showTable();
});
var table;

function showTable(){
   table = $('#attendance-table').dataTable({

        "destroy" : true
   });
}
jQuery('#credit_discredit_ending_date').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd',
    todayHighlight: true,
    endDate: '+0d'
}).on('changeDate', function (ev) {
    showTableOne();
});
var table;

function showTableOne(){
   table = $('#attendance-table').dataTable({

        "destroy" : true
   });
}
function checkstart(){
     var x = $.datepicker.formatDate("yy-mm-dd", new Date($('#credit_discredit_starting_date').val()));
     var y = $.datepicker.formatDate("yy-mm-dd", new Date($('#credit_discredit_ending_date').val()));

     if(x>y){
          $('#credit_discredit_ending_date').val(x);
     }

}
function checkend(){
     var x = $.datepicker.formatDate("yy-mm-dd", new Date($('#credit_discredit_starting_date').val()));
     var y = $.datepicker.formatDate("yy-mm-dd", new Date($('#credit_discredit_ending_date').val()));

     if(x>y){
          $('#credit_discredit_starting_date').val(y);
     }
}

</script>
@if(isset($user_id))
<script>
$('#employeeId').val(<?php echo $user_id ?>);
</script>
@endif
@if($errors->any())
<script>
$('.success_msg').css("display", "block");;
      $('.success_msg').delay(2000).fadeOut('slow');
                setTimeout(function() {
                     $(".success_msg").modal('hide');
      }, 2500);
      </script>
@endif

@endpush
