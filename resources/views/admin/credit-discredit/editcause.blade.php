@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.credit-discredit') }}">{{ $pageTitle }}</a></li>
                <li class="active">Edit Cause</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
@endpush

@section('content')

    <div class="row">

        <div class="col-md-12">


             <div class="panel panel-inverse">
                 <div class="panel-heading"> Add Cause</div>
                 <div class="panel-wrapper collapse in" aria-expanded="true">
                     <div class="panel-body">
                        <form method="GET" action="/admin/credit-discredit/updatecause" >
                             <input type="hidden" name="id" value="{{$id}}">
                         <div class="form-body">

                              @php
                              $type=DB::table('credit_discredit_cause')->where('id',$id)->get();
                              @endphp
                              @if(isset($type))
                                   @if(count($type)>0)
                             <div class="row">
                                 <div class="col-md-6 ">
                                     <div class="form-group">
                                          <label class="control-label">Select Credit/Discredit</label>

                                          <select class="selectpicker form-control" name="type"  id="user_id" data-style="form-control">
                                                 <option value="1">{{"Credit"}}</option>
                                                 <option value="2">{{"Discredit"}}</option>

                                         </select>
                                     </div>
                                 </div>
                                 <div class="col-md-6">
                                 </div>
                             </div>
                             <div class="row">
                                  <div class="col-xs-12">
                                     <div class="form-group">
                                         <label class="control-label">Cause Details</label>
                                         <textarea  id="cause_details" name="cause_details"  id="notes" rows="5"  class="form-control"></textarea>
                                     </div>
                                 </div>

                             </div>

                        @endif
                        @endif
                             <div class="row">

                                 <!--/span-->


                                 <!--/span-->
                             </div>
                             <!--/row-->


                         </div>
                         <div class="form-actions">
                             <button type="submit" id="save-form" class="btn btn-success"><i class="fa fa-check"></i>
                                 Update
                             </button>
                         </div>
                    </form>
                     </div>
                 </div>
             </div>

          </div>
        </div>
    </div>    <!-- .row -->

  @endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>

<script>
$('#user_id').val(<?php echo $type[0]->type ?>);
$('#cause_details').val(<?php echo "'".$type[0]->cause_details."'" ?>);
</script>

@endpush
