@extends('layouts.member-app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('member.credit-discredit') }}">{{ $pageTitle }}</a></li>
                <li class="active">{{'Edit'}}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
@endpush

@section('content')

    <div class="row">

        <div class="col-md-12">


            <div class="panel panel-inverse">
                <div class="panel-heading"> Edit Credit/Discredit</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                       <form method="GET" action="/member/credit-discredit/update" >
                            <input type="hidden" name="id" value="{{ $id }}">
                        <div class="form-body">
                             @php
                            $credit_discredit=DB::table('employee_credit_discredit')->where('id',$id)->get();
                            @endphp
                            @if(isset($credit_discredit))
                               @if(count($credit_discredit)>0)
                                    <input type="hidden" name="type" value="{{ $credit_discredit[0]->type }}">

                            <div class="row">
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                         <label class="control-label">Select  Employee</label>
                                         @php
                                        $employee_id=DB::table('role_user')->where('role_id',2)->orderBy('user_id','ASC')->get();
                                        @endphp
                                        @if(isset($employee_id))
                                            @if(count($employee_id)>0)
                              <select class="selectpicker form-control" name="user_id" value="{{ $credit_discredit[0]->user_id }}" id="user_id"
                                                data-style="form-control">
                                            @foreach($employee_id as $employee_id)
                                                 @php
                                                 $users=DB::table('users')->where('id',$employee_id->user_id)->get();
                                                 @endphp
                                                 @if(isset($users))
                                                      @if(count($users)>0)
                                                <option value="{{ $users[0]->id }}">{{$users[0]->name }}</option>

                                           @endif
                                           @endif

                                            @endforeach
                                        </select>
                                   @else

                                   @endif
                                   @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 ">
                                    <div class="form-group">
                                        <label class="control-label">Select  Cause</label>
                                        @php
                                       $causes=DB::table('credit_discredit_cause')->where('type',$credit_discredit[0]->type)->get();
                                       @endphp
                                       <select class="selectpicker form-control" name="cause" id="cause_id"
                                               data-style="form-control">
                                               @if(isset($causes))
                                                    @if(count($causes)>0)
                                           @foreach($causes as $cause)
                                               <option value="{{ $cause->id }}">{{$cause->cause_details }}</option>


                                           @endforeach
                                      @else


                                      @endif
                                 @endif
                                       </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                               <div class="col-xs-12">
                                   <div class="form-group">
                                       <label class="control-label">Specific  Cause</label>
                                       <textarea name="specific_cause" id="notes" rows="2" class="form-control" value="{{$credit_discredit[0]->specific_cause}}"></textarea>
                                   </div>
                               </div>

                          </div>
                            <div class="row">



                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Add Point</label>
                                        <input type="number" name="point" value="{{ $credit_discredit[0]->point }}" required class="form-control">
                                    </div>
                                </div>
                                <!--/span-->


                                <!--/span-->
                            </div>
                            <!--/row-->
                       @endif
                       @endif


                        </div>
                        <div class="form-actions">
                            <button type="submit" id="save-form" class="btn btn-success"><i class="fa fa-check"></i>
                                Update
                            </button>
                        </div>
                   </form>
                    </div>
                </div>
            </div>

          </div>
        </div>
    </div>    <!-- .row -->

  @endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
<script>
$('#notes').val('<?php echo $credit_discredit[0]->specific_cause ?>');
$('#cause_id').val(<?php echo $credit_discredit[0]->cause ?>);
$('#user_id').val(<?php echo $credit_discredit[0]->user_id ?>);
</script>


@endpush
