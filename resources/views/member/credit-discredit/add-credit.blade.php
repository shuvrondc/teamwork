@extends('layouts.member-app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('member.credit-discredit') }}">{{ $pageTitle }}</a></li>
                <li class="active">Add Credit</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
@endpush

@section('content')

    <div class="row">
         <div class="col-md-8">
         </div>
         <div class="col-md-4">

            <div class="jq-toast-wrap top-right">
                 <div class="success_msg jq-toast-single jq-has-icon jq-icon-success" style="text-align: left; display: none;">
                      <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 3.1s ease-in;-o-transition: width 3.1s ease-in;transition: width 3.1s ease-in;background-color: #ff6849;"></span>
                      @if($errors->any())
                           {{$errors->first()}}
                      @endif
            </div>

         </div>

         </div>
         <div class="col-sm-6">
            <div class="form-group">
                <a href="/member/credit-discredit/add-cause" class="btn btn-success btn-sm">Add Cause <i class="fa fa-plus" aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="col-md-12">


            <div class="panel panel-inverse">
                <div class="panel-heading"> Add Credit</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                       <form method="GET" action="/member/credit-discredit/add" >
                            <input type="hidden" name="type" value="1">
                        <div class="form-body">

                            <div class="row">
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                         <label class="control-label">Select  Employee</label>
                                         @php
                                        $employee_id=DB::table('role_user')->where('role_id',2)->orderBy('user_id','ASC')->get();
                                        @endphp
                                        @if(isset($employee_id))
                                            @if(count($employee_id)>0)
                                        <select class="selectpicker form-control" name="user_id" id="category_id"
                                                data-style="form-control">
                                            @foreach($employee_id as $employee_id)
                                                 @php
                                                 $users=DB::table('users')->where('id',$employee_id->user_id)->get();
                                                 @endphp
                                                 @if(isset($users))
                                                      @if(count($users)>0)
                                                <option value="{{ $users[0]->id }}">{{$users[0]->name }}</option>
                                           @endif
                                           @endif

                                            @endforeach
                                        </select>
                                   @else

                                   @endif
                                   @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 ">
                                    <div class="form-group">
                                        <label class="control-label">Select  Cause</label>
                                        @php
                                       $causes=DB::table('credit_discredit_cause')->where('type',1)->get();
                                       @endphp
                                       <select class="selectpicker form-control" name="cause" id="category_id"
                                               data-style="form-control">
                                               @if(isset($causes))
                                                    @if(count($causes)>0)
                                           @foreach($causes as $cause)
                                               <option value="{{ $cause->id }}">{{$cause->cause_details }}</option>

                                           @endforeach
                                      @else


                                      @endif
                                 @endif
                                       </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                               <div class="col-xs-12">
                                   <div class="form-group">
                                       <label class="control-label">Specific  Cause</label>
                                       <textarea name="specific_cause" id="notes" rows="2" class="form-control"></textarea>
                                   </div>
                               </div>

                          </div>
                            <div class="row">



                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Add Point</label>
                                        <input type="number" value="1" min="1" name="point" required class="form-control">
                                    </div>
                                </div>
                                <!--/span-->


                                <!--/span-->
                            </div>
                            <!--/row-->


                        </div>
                        <div class="form-actions">
                            <button type="submit" id="save-form" class="btn btn-success"><i class="fa fa-check"></i>
                                Add
                            </button>
                        </div>
                   </form>
                    </div>
                </div>
            </div>
            <div class="panel panel-inverse">
                <div class="panel-heading"> Recently Added Credits</div>

            <div class="white-box">

                  @php
                  $causes=DB::table('employee_credit_discredit')->where('type',1)->take(5)->orderBy('created_at','Desc')->get();
                  @endphp
               @if(isset($causes))
               @if(count($causes)>0)


                <div class="table-responsive">
                    <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="users-table">
                        <thead>
                        <tr>
                           <th>User Name</th>
                           <th>Credit</th>
                           <th>Point</th>
                           <th>Given By</th>
                           <th>Cause</th>
                           <th>Date</th>
                           <th>@lang('app.action')</th>
                        </tr>
                        </thead>
                        @foreach($causes as $cause)

                        <tr>
                             <td>
                                  @php
                                  $employee=DB::table('users')->where('id',$cause->user_id)->get();
                                  @endphp
                                   @if(isset($employee))
                                       @if(count($employee)>0){{$employee[0]->name }}@endif @endif
                             </td>
                             <td>@if($cause->type==2)<label class="label label-danger">Discredit</label>@elseif($cause->type==1)<label class="label label-success">Credit</label>@endif</td>
                              <td>{{ $cause->point }}</td>
                              <td>{{ $cause->given_by }}</td>
                              <td>
                                   @php
                                   $cause_text=DB::table('credit_discredit_cause')->where('id',$cause->cause)->get();
                                   @endphp
                                   @if(isset($cause_text))
                                       @if(count($cause_text)>0){{$cause_text[0]->cause_details }}@endif @endif
                                   </td>
                              <td>
                                   @php $date = new DateTime($cause->created_at);$date = $date->format('d-m-Y');@endphp{{$date}}
                              </td>
                              <td>
                                   @if($user->can('edit_credit_discredit'))
                                        <a href="/member/credit-discredit/edit/{{ $cause->id }}" class="btn btn-info btn-circle" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                   @endif
                                   @if($user->can('delete_credit_discredit'))
                      <a href="/member/credit-discredit/delete/{{ $cause->id }}" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn-circle sa-params" data-toggle="tooltip" data-user-id="1" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                 @endif
            </td>

                        </tr>
                         @endforeach

                    </table>
                </div>
               @endif
               @endif
               </div>
          </div>
          </div>
        </div>
    </div>    <!-- .row -->

  @endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
@if($errors->any())
<script>
$('.success_msg').css("display", "block");;
      $('.success_msg').delay(2000).fadeOut('slow');
                setTimeout(function() {
                     $(".success_msg").modal('hide');
      }, 2500);
      </script>
@endif

@endpush
