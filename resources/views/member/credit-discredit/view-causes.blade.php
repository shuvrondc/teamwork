@extends('layouts.member-app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('member.credit-discredit') }}">{{$pageTitle}}</a></li>
                <li class="active">{{ 'View Causes' }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush

@section('content')

    <div class="row">

         <div class="col-md-12">

              <div class="white-box p-b-0 bg-inverse text-white">
                 <div class="row">
                      <form method="GET" action="/member/credit-discredit-cause/{{ $id }}" >

                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="control-label">Starting Date</label>
                                  <input type="text" class="form-control" name="starting_date" id="credit_discredit_starting_date" value="@if(isset($starting_date)){{ $starting_date }}@else{{ Carbon\Carbon::today()->format('Y-m-d') }}@endif" onchange="checkstart()">
                              </div>

                          </div>
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="control-label">Ending Date</label>
                                  <input type="text" class="form-control" name="ending_date" id="credit_discredit_ending_date" value="@if(isset($ending_date)){{ $ending_date }}@else{{ Carbon\Carbon::today()->format('Y-m-d') }}@endif" onchange="checkend()">
                              </div>

                          </div>

                          <div class="col-md-3">
                               <div class="form-group m-t-25">
                                    <button type="submit" id="apply-filter" class="btn btn-success btn-block">Apply</button>
                               </div>
                          </div>
                     </form>



                 </div>

             </div>
             <div class="white-box">


                  <div class="table-responsive">
                       <div class="white-box">
                            <div class="row">
                                 <div class="col-md-3"></div>
                                 <div class="col-md-6">
                                      <div class="col-md-4"><b> User Id :</b> <label class="label label-success"> {{ $id }}</label></div>
                                      @php
                                      $employee=DB::table('users')->where('id',$id)->get();
                                      @endphp
                                      <div class="col-md-8"> <b> User Name : </b> @if(isset($employee))
                                           @if(count($employee)>0)<label class="label label-primary">{{$employee[0]->name }}</label>@endif @endif </div>

                                           </div>
                                      </div>
                                 </div>
                                 <div class="white-box">
                                    <div class="row">


                        <div class="col-md-3">

                        </div>

                        <div class="col-md-3">
                            <div class="white-box bg-success">
                                <h3 class="box-title text-white">Total Credit</h3>
                                <ul class="list-inline two-part">
                                    <li><i class="icon-clock text-white"></i></li>
                                    <li class="text-right"><span id="daysLate" class="counter text-white">
                                         @php
                                              $credit_point=0;
                                         @endphp
                                         @if(isset($starting_date))

                                        @if(isset($ending_date))
                                             @php
                                             $credit=DB::table('employee_credit_discredit')->where('user_id',$id)->where('type',1)->where('created_at','>=',$starting_date.' 00:00:00')->where('created_at','<=',$ending_date.' 23:59:59')->get();
                                             @endphp
                                        @else
                                             @php
                                             $credit=DB::table('employee_credit_discredit')->where('user_id',$id)->where('type',1)->get();
                                             @endphp


                                        @endif
                                        @else
                                             @php
                                             $credit=DB::table('employee_credit_discredit')->where('user_id',$id)->where('type',1)->get();
                                             @endphp
                                        @endif
                                        @if(isset($credit))
                                        @if(count($credit)>0)
                                         @foreach($credit as $credit)
                                              @php
                                              $credit_point=$credit_point+$credit->point;
                                              @endphp
                                         @endforeach
                                           @endif
                                           @endif
                                           {{$credit_point}}
                                    </span></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="white-box bg-danger">
                                <h3 class="box-title text-white">Total Discredit</h3>
                                <ul class="list-inline two-part">
                                    <li><i class="icon-clock text-white"></i></li>
                                    <li class="text-right"><span id="halfDays" class="counter text-white">
                                         @php
                                              $discredit_point=0;
                                         @endphp

                                                                      @if(isset($starting_date))

                                                                   @if(isset($ending_date))
                                                                        @php
                                                                       $discredit=DB::table('employee_credit_discredit')->where('user_id',$id)->where('type',2)->where('created_at','>=',$starting_date.' 00:00:00')->where('created_at','<=',$ending_date.' 23:59:59')->get();
                                                                        @endphp
                                                                   @else
                                                                        @php
                                                                       $discredit=DB::table('employee_credit_discredit')->where('type',2)->where('user_id',$id)->get();
                                                                        @endphp


                                                                   @endif
                                                                   @else
                                                                        @php
                                                                        $discredit=DB::table('employee_credit_discredit')->where('type',2)->where('user_id',$id)->get();
                                                                        @endphp
                                                                   @endif
                                                                   @if(isset($discredit))
                                                                   @if(count($discredit)>0)

                                                                     @foreach($discredit as $discredit)
                                                                          @php
                                                                          $discredit_point=$discredit_point+$discredit->point;
                                                                          @endphp
                                                                     @endforeach
                                                                @endif
                                                                @endif
                                                                {{$discredit_point}}
                                    </span></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-3">

                        </div>

                    </div>
                                        </div>

                 @if(isset($starting_date))

              @if(isset($ending_date))
                   @php
                  $causes=DB::table('employee_credit_discredit')->where('user_id',$id)->where('created_at','>=',$starting_date.' 00:00:00')->where('created_at','<=',$ending_date.' 23:59:59')->get();
                   @endphp
              @else
                   @php
                  $causes=DB::table('employee_credit_discredit')->where('user_id',$id)->get();
                   @endphp


              @endif
              @else
                   @php
                   $causes=DB::table('employee_credit_discredit')->where('user_id',$id)->get();
                   @endphp
              @endif
              @if(isset($causes))
              @if(count($causes)>0)




                    <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="users-table">
                        <thead>
                        <tr>
                            <th>Credit/Discredit</th>
                            <th>Point</th>
                            <th>Cause</th>
                            <th>Date</th>
                            <th>@lang('app.action')</th>
                        </tr>
                        </thead>
                        @foreach($causes as $cause)

                        <tr>
                             <td>@if($cause->type==2)<label class="label label-danger">Discredit</label>@elseif($cause->type==1)<label class="label label-success">Credit</label>@endif</td>
                              <td>{{ $cause->point }}</td>
                              <td>
                                   @php
                                   $cause_text=DB::table('credit_discredit_cause')->where('id',$cause->cause)->get();
                                   @endphp
                                   @if(isset($cause_text))
                                       @if(count($cause_text)>0){{$cause_text[0]->cause_details }}@endif @endif
                                            @if(is_null($cause->specific_cause))

                                            @else
                                              <div>   <h6 class="label label-primary">{{$cause->specific_cause }}</h6> </div>
                                            @endif
                                   </td>
                              <td>
                                   @php $date = new DateTime($cause->created_at);$date = $date->format('d-m-Y');@endphp{{$date}}
                              </td>
                              <td>

                                   @if($user->can('edit_credit_discredit'))
                                        <a href="/member/credit-discredit/edit/{{ $cause->id }}" class="btn btn-info btn-circle" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                   @endif
                                   @if($user->can('delete_credit_discredit'))
                      <a href="/member/credit-discredit/delete/{{ $cause->id }}" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn-circle sa-params" data-toggle="tooltip" data-user-id="1" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                 @endif

                 </td>

                        </tr>
                         @endforeach

                    </table>
                </div>
           @endif
           @endif
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script>
jQuery('#credit_discredit_starting_date').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd',
    todayHighlight: true,
    endDate: '+0d'
}).on('changeDate', function (ev) {
    showTable();
});
var table;

function showTable(){
   table = $('#attendance-table').dataTable({

        "destroy" : true
   });
}
jQuery('#credit_discredit_ending_date').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd',
    todayHighlight: true,
    endDate: '+0d'
}).on('changeDate', function (ev) {
    showTableOne();
});
var table;

function showTableOne(){
   table = $('#attendance-table').dataTable({

        "destroy" : true
   });
}
function checkstart(){
     var x = $.datepicker.formatDate("yy-mm-dd", new Date($('#credit_discredit_starting_date').val()));
     var y = $.datepicker.formatDate("yy-mm-dd", new Date($('#credit_discredit_ending_date').val()));

     if(x>y){
          $('#credit_discredit_ending_date').val(x);
     }

}
function checkend(){
     var x = $.datepicker.formatDate("yy-mm-dd", new Date($('#credit_discredit_starting_date').val()));
     var y = $.datepicker.formatDate("yy-mm-dd", new Date($('#credit_discredit_ending_date').val()));

     if(x>y){
          $('#credit_discredit_starting_date').val(y);
     }
}
</script>

@endpush
